Public Function Ping(ByVal ComputerName As String) As Boolean
'       return TRUE, if ping succeded
    If (ComputerName <> "") Then
        Dim oPingResult As Variant
        For Each oPingResult In GetObject("winmgmts://./root/cimv2").ExecQuery _
            ("SELECT * FROM Win32_PingStatus WHERE Address = '" & ComputerName & "'")
            If IsObject(oPingResult) Then
                If oPingResult.StatusCode = 0 Then
                    Ping = True        'Debug.Print "ResponseTime", oPingResult.ResponseTime
                    Exit Function
               End If
           End If
       Next
    End If
End Function

Sub TestPingFunction()
    
    Dim ShGeneral As Worksheet
    Dim ListObj As ListObject
    
    Set ShGeneral = ThisWorkbook.Worksheets("Sheet1")
    Set ListObj = ShGeneral.ListObjects("MainTable")
    
    Dim oSelection
    Set oSelection = Selection
    
    For Each ComputerName In oSelection

        If Ping(ComputerName) Then
            ' MsgBox ComputerName & " success"
            ' MainTableFunc (True)
            ListObj.ListRows.Item(ComputerName.Row - 1).Range(1, 2).Interior.Color = vbGreen
            
        End If

        If Not Ping(ComputerName) Then
            ' MsgBox ComputerName & " fail"
            ' MainTableFunc (False)
            ListObj.ListRows.Item(ComputerName.Row - 1).Range(1, 2).Interior.Color = vbRed
            
        End If
    Next
End Sub



