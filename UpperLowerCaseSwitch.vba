Function ToUpper(ByVal Cells As Range)
    
    For Each Cell In Cells
        Cell.Value = UCase(Cell)
    Next Cell

End Function
Function ToLower(ByVal Cells As Range)
    
    For Each Cell In Cells
        Cell.Value = LCase(Cell)
    Next Cell
    
End Function


Sub CaseSwitch()

    Dim rng As Range
    Set rng = Selection
    For Each Cell In rng
        If Cell.Value = UCase(Cell.Value) Then
            Result = ToLower(rng)
        Else
            Result = ToUpper(rng)
        End If
        Exit For
    Next Cell
End Sub
