' ################################################################

' Module

' ################################################################

Private Sub bAvgD_Click()

    AMI ("R3C18")

    ' красим кнопки
    Me.bIndivND.BackColor = RGB(200, 200, 200)
    Me.bMaxND.BackColor = RGB(200, 200, 200)
    Me.bAvgND.BackColor = RGB(200, 200, 200)
    Me.bAvgD.BackColor = RGB(50, 200, 70)

End Sub

Private Sub bAvgND_Click()

    AMI ("R3C17")
    
    ' красим кнопки
    Me.bIndivND.BackColor = RGB(200, 200, 200)
    Me.bMaxND.BackColor = RGB(200, 200, 200)
    Me.bAvgND.BackColor = RGB(50, 200, 70)
    Me.bAvgD.BackColor = RGB(200, 200, 200)
    
End Sub

Private Sub bDefault_Click()
    
    DfltMrkt ("R3C4")
    
    ' красим кнопки
    Me.bDefault.BackColor = RGB(50, 200, 70)
    Me.bMarket.BackColor = RGB(200, 200, 200)

End Sub

Private Sub bIndivND_Click()

    AMI ("R5C17")
    
    ' красим кнопки
    Me.bIndivND.BackColor = RGB(50, 200, 70)
    Me.bMaxND.BackColor = RGB(200, 200, 200)
    Me.bAvgND.BackColor = RGB(200, 200, 200)
    Me.bAvgD.BackColor = RGB(200, 200, 200)

End Sub

Private Sub bMarket_Click()
    
    DfltMrkt ("R3C8")
    
    ' красим кнопки
    Me.bDefault.BackColor = RGB(200, 200, 200)
    Me.bMarket.BackColor = RGB(50, 200, 70)

    
End Sub

Private Sub bMaxND_Click()

    AMI ("R4C17")
    
    ' красим кнопки
    Me.bIndivND.BackColor = RGB(200, 200, 200)
    Me.bMaxND.BackColor = RGB(50, 200, 70)
    Me.bAvgND.BackColor = RGB(200, 200, 200)
    Me.bAvgD.BackColor = RGB(200, 200, 200)

End Sub



' ################################################################

' Module

' ################################################################
Private Function Slide(a As String, b As String) As String

    Dim d1 As Integer
    Dim d2 As Integer
    Dim arr() As String
    
    ' выполняем разделение по букве C в полученном абсолютном адресе ячейки и получаем массив с 2умя строками
        arr = Split(a, "C")
    
    ' преобразуем в число и записываем 2ой объект массива в d1
        d1 = CInt(arr(1))

    ' выполняем разделение по букве C в полученном абсолютном адресе ячейки и получаем массив с 2умя строками
        arr = Split(b, "C")
    
    ' преобразуем в число и записываем 2ой объект массива в d2
        d2 = CInt(arr(1))
        
    ' вычисляем сдвиг, преобразуем его в строку и присваиваем как результат переменной Slide
        Slide = CStr(d1 - d2)

End Function

Private Function GetAddr(colName As String, objTable As ListObject) As String

    ' определяем адрес ячейки с ценой (Fixed Net Selling Price RU)
        Set objCell = objTable.ListColumns(colName).Range(2)
        GetAddr = objCell.Address(ReferenceStyle:=xlR1C1)

End Function

Public Function DfltMrkt(cellAddrssFXrt As String) As Boolean

    Dim wrkshtName        As String
    Dim tableName         As String
    Dim formula           As String
    Dim cellAddrssFrml(2) As String
    Dim colNmsFrml(2)     As String
    Dim cellAddrssPrc(2)  As String
    Dim colNmsPrc(2)      As String
    Dim objCol            As Range
    Dim objCell           As Range
    Dim objTable          As ListObject
    Dim objLstCol         As ListColumn
    Dim i                 As Integer

    ' задаем базовые переменные (имя листа, имя таблицы, имена колонок для поиска)
        wrkshtName = "RU Range & Inco Price"
        tableName = "MainTable"
    
    ' имена колонок для записи формулы
        colNmsFrml(0) = "EU Landed Cost FY20 (RUB)"
        colNmsFrml(1) = "Fixed Net Selling Price RU (RUB)"
    
    ' переменные для кнопки "Default (Internal)"
        colNmsPrc(0) = "EU Landed Cost FY20"
        colNmsPrc(1) = "Fixed Net Selling Price RU"
            
    ' переменная таблицы
        Set objTable = Worksheets(wrkshtName).ListObjects(tableName)
        
' #############################################################################################

    i = 0
    Do While i <= 1

    ' переменная колонок таблицы
        Set objLstCol = objTable.ListColumns(colNmsFrml(i))

    ' определяем адрес ячейки с ценой ("EU Landed Cost FY20" на первом шаге цикла и "Fixed Net Selling Price RU" на втором)
        cellAddrssPrc(i) = GetAddr(colNmsPrc(i), objTable)
        
    ' определяем адрес первой ячейки куда будет вставлена формула (EU Landed Cost FY20 (RUB))
        cellAddrssFrml(i) = GetAddr(colNmsFrml(i), objTable)

    ' вычисляем сдвиг от ячейки с формулой до ячейки с ценой (EU Landed Cost FY20) используя функцию Slide объявленную выше.
    ' и генерируем формулу для ячейки с относительными ссылками типа:
    ' "=IFERROR(RC[-5]*R3C4,""-"")"
        
        formula = "=IFERROR(RC[-" & Slide(cellAddrssFrml(i), cellAddrssPrc(i)) & "]*" & cellAddrssFXrt & ",""""-" & """"")"
        
    ' записываем формулу в ячейку выполняем автозаполнение по всему столбцу
        Set objCell = objLstCol.Range(i + 2)
        Set objCol = objLstCol.DataBodyRange
        objCell.FormulaR1C1 = formula
        objCol.FillDown
        i = i + 1
     Loop

End Function


Public Function AMI(cellAddrFXrt As String) As Boolean

    Dim wrkshtName        As String
    Dim tableName         As String
    Dim formula           As String
    Dim cellAddrFrml      As String
    Dim colNmFrml         As String
    Dim cellAddrPrc       As String
    Dim colNmPrc          As String
    Dim objCol            As Range
    Dim objCell           As Range
    Dim objTable          As ListObject
    Dim objLstCol         As ListColumn
    Dim objWrksht         As Worksheet
    
    ' задаем базовые переменные (имя листа, имя таблицы, имена колонок для поиска)
        wrkshtName = "RU Range & Inco Price"
        tableName = "MainTable"
        
    ' имена колонок для записи формулы
        colNmFrml = "1C Current Net Selling Price RU"
            
    ' переменные для кнопки "Default (Internal)"
        colNmPrc = "1C Current Gross Selling Price RU"
  
    ' переменная листа, таблицы, колонки
        Set objWrksht = Worksheets(wrkshtName)
        Set objTable = objWrksht.ListObjects(tableName)
        Set objLstCol = objTable.ListColumns(colNmFrml)
        
    ' определяем адрес ячейки с ценой (EU Landed Cost FY20)
        cellAddrPrc = GetAddr(colNmPrc, objTable)

    ' определяем адрес первой ячейки куда будет вставлена формула (EU Landed Cost FY20 (RUB))
        cellAddrFrml = GetAddr(colNmFrml, objTable)

    ' вычисляем сдвиг от ячейки с формулой до ячейки с ценой (EU Landed Cost FY20) используя функцию Slide объявленную выше.
    ' и генерируем формулу для ячейки с относительными ссылками типа:
    ' "=IFERROR(RC[1]*(1-R2C10),"")"
        
        formula = "=IFERROR(RC[" & Slide(cellAddrPrc, cellAddrFrml) & "]*(1-" & cellAddrFXrt & "),""""-" & """"")"

    ' записываем формулу в ячейку выполняем автозаполнение по всему столбцу
        Set objCell = objLstCol.Range(2)
        Set objCol = objLstCol.DataBodyRange
        objCell.FormulaR1C1 = formula
        objCol.FillDown
        
End Function
